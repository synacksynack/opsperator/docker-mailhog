FROM docker.io/golang:alpine AS builder

# MailHog image for OpenShift Origin

ENV MAILHOG_VERSION=1.0.1

LABEL io.k8s.description="MailHog $MAILHOG_VERSION Image." \
      io.k8s.display-name="MailHog" \
      io.openshift.expose-services="1025:smtp,8025:http" \
      io.openshift.tags="mailhog" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mailhog" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MAILHOG_VERSION"

RUN set -x \
    && apk --no-cache add --virtual build-dependencies git \
    && mkdir -p /root/gocode \
    && export GOPATH=/root/gocode \
    && go install github.com/mailhog/MailHog@v$MAILHOG_VERSION

FROM docker.io/alpine:3

COPY --from=builder /root/gocode/bin/MailHog /usr/local/bin/

RUN set -x \
    && adduser -D -u 1000 mailhog \
    && mkdir -p /home/mailhog \
    && chown 1000:0 /home/mailhog \
    && chmod 775 /home/mailhog

USER 1000
WORKDIR /home/mailhog
ENTRYPOINT ["/usr/local/bin/MailHog"]
